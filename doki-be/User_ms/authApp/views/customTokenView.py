from rest_framework_simplejwt.views import TokenObtainPairView
from authApp.serializers.customTokenSerializer import CustomTokenSerializer

class CustomTokenView(TokenObtainPairView):
    serializer_class = CustomTokenSerializer
    token_obtain_pair = TokenObtainPairView.as_view()