from rest_framework import generics

from authApp.models.user import User
from authApp.serializers.userSerializer import UserSerializer

class UserUpdateView(generics.UpdateAPIView):
    queryset = User.objects.clients()
    serializer_class = UserSerializer
    success_message = "Actualizado con exito"
    lookup_field = 'username'

    # def put(self, request, *args, **kwargs):     
    #     user_data = request.data
    #     user_serializer = UserSerializer(User.objects.clients().get(username=user_data.username), data=user_data)
    #     user_serializer.save(user_serializer.data) 
    #     return JsonResponse(userSerializer.data)