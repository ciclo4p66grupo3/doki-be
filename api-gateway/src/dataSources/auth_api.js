const { RESTDataSource } = require('apollo-datasource-rest');
const serverConfig = require('../server');
class AuthAPI extends RESTDataSource {
    constructor() {
        super();
        this.baseURL = serverConfig.auth_api_url;
    }
    async createUser(user) {
        user = new Object(JSON.parse(JSON.stringify(user)));
        return await this.post(`/user/`, user);
    }
    async getUser(userId) {
        return await this.get(`/user/${userId}/`);
    }
    async authRequest(credentials) {
        credentials = new Object(JSON.parse(JSON.stringify(credentials)));
        return await this.post(`/login/`, credentials);
    }
    async refreshToken(token) {
        token = new Object(JSON.parse(JSON.stringify({ refresh: token })));
        return await this.post(`/refresh/`, token);
    }
    async getAllClients() {
        return await this.get(`/all-clients`);
    }
    async getAllUsers() {
        return await this.get(`/all-users`);
    }
    // Add another param to the funtion for endpoint userName access
    async updateUser(userId, user) {
        user = new Object(JSON.parse(JSON.stringify(user)));
        return await this.put(`/update-user/${userId}/`, user);
    }
    async deleteUser(userId) {
        return await this.delete(`/delete-user/${userId}/`);
    }
}
module.exports = AuthAPI;