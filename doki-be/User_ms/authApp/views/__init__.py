from .userCreateView import UserCreateView
from .userDetailView import UserDetailView
from .verifyTokenView import VerifyTokenView
from .clientsListView import ClientsListView
from .usersListView import UsersListView
from .userDeleteView import UserDeleteView
from .userUpdateView import UserUpdateView
from .customTokenView import CustomTokenView