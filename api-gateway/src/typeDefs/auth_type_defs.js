const { gql } = require('apollo-server');
const authTypeDefs = gql`
    type Tokens {
        refresh: String!
        access: String!
    }
    type Access {
        access: String!
    }
    input CredentialsInput {
        username: String!
        password: String!
    }
    input SignUpInput {
        username: String!
        password: String!
        name: String!
        email: String!
        role: String
    }
    type UserDetail {
        id: Int!
        username: String!
        password: String!
        name: String!
        email: String!
        role: String!
    }

    type Mutation {
        signUpUser(userInput :SignUpInput): Tokens!
        logIn(credentials: CredentialsInput!): Tokens!
        refreshToken(refresh: String!): Access!
        userUpdate(userName: String!, userInfo : SignUpInput): UserDetail!
        userDelete(userName: String!): String!
    }
    type Query {
        userDetailByUsername(userName: String!): UserDetail!
        allClients:[UserDetail]
        allUsers:[UserDetail]
    }
`;
module.exports = authTypeDefs;