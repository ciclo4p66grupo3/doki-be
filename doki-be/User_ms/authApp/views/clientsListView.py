from rest_framework import generics

from authApp.models.user import User
from authApp.serializers.userSerializer import UserSerializer

class ClientsListView(generics.ListAPIView):
    queryset = User.objects.clients()
    serializer_class = UserSerializer