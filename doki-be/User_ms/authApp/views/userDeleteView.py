from rest_framework import generics, status
from rest_framework.response import Response
from authApp.models.user import User
from authApp.serializers.userSerializer import UserSerializer

class UserDeleteView(generics.DestroyAPIView):
    queryset = User.objects.clients()
    serializer_class = UserSerializer
    success_message = "Borrado con exito"
    lookup_field = 'username'

    ## Code bellow not working on Heroku, Server wont response on Delete requests
    # def destroy(self, request, *args, **kwargs):
    #     instance = self.get_object()
    #     data = {"state":self.perform_destroy(instance), "msg":"Borrado con exito"}
    #     return Response({"msg": "Borrado con exito"}, status=status.HTTP_204_NO_CONTENT)

    # def get_queryset(self):
    #     owner = self.request.user
    #     return self.model.objects.filter(owner=owner)

