from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, BaseUserManager
from django.contrib.auth.hashers import make_password


class UserManager(BaseUserManager):
    def create_user(self, username, password=None, role='C'):
        """
        Creates and saves a user with the given username and password.
        """
        if not username:
            raise ValueError('Users must have an username')
        user = self.model(username=username)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, username, password, role='A'):
        """
        Creates and saves a superuser with the given username and password.
        """
        user = self.create_user(
            username=username,
            password=password,
        )
        user.is_admin = True
        user.save(using=self._db)
        return user

    def clients(self):
        return self.filter(role='C')

    def admins(self):
        return self.filter(role='A')

class User(AbstractBaseUser, PermissionsMixin):
    #id = models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')#unique=True
    username = models.CharField('Username', max_length = 15, unique=True)
    password = models.CharField('Password', max_length = 256)
    name = models.CharField('Name', max_length = 30)
    email = models.EmailField('Email', max_length = 100)
    role = models.CharField('Role', max_length=7, default='C',choices=[('A','Administrador'), ('C', 'Cliente')])

    def save(self, **kwargs):
        some_salt = 'mMUj0DrIK6vgtdIYepkIxN' 
        self.password = make_password(self.password, some_salt)
        super().save(**kwargs)

    objects = UserManager()
    USERNAME_FIELD = 'username'
    # REQUIRED_FIELDS = ['password', 'name', 'email']
    # is_anonymous = False
    # is_authenticated = False
