const usersResolver = {
    Query: {
        userDetailByUsername: (_, { userName }, { dataSources, userIdToken, userRoleToken }) => {
            if (userName == userIdToken || userRoleToken == "A")
                return dataSources.authAPI.getUser(userName)
            else
                return null

        },
        allClients: (_, {  }, { dataSources }) => 
            dataSources.authAPI.getAllClients(),
        allUsers: (_, { }, {dataSources}) =>
            dataSources.authAPI.getAllUsers(),

    },
    Mutation: {
        signUpUser: async (_, { userInput }, { dataSources }) => {
            const authInput = {
                username: userInput.username,
                password: userInput.password,
                name: userInput.name,
                email: userInput.email,
            }
            return await dataSources.authAPI.createUser(authInput);
        },
        logIn: (_, { credentials }, { dataSources }) =>
            dataSources.authAPI.authRequest(credentials),
        refreshToken: (_, { refresh }, { dataSources }) =>
            dataSources.authAPI.refreshToken(refresh),
        userUpdate: async (_, { userName, userInfo }, { dataSources }) =>
            await dataSources.authAPI.updateUser(userName, userInfo),
        userDelete: async (_, { userName }, { dataSources }) =>
            await dataSources.authAPI.deleteUser(userName),
    

    }
};
module.exports = usersResolver;