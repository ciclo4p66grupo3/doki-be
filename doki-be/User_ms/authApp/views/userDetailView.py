from rest_framework import generics
from django.shortcuts import get_object_or_404
from authApp.models.user import User
from authApp.serializers.userSerializer import UserSerializer

class UserDetailView(generics.RetrieveAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    lookup_field = 'username'

# def get_object(self):
#     UserName= self.kwargs.get("username")
#     return get_object_or_404(User, username=UserName)